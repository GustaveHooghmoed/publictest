package me.rides.Rides.Symbolica.Manager;

import java.io.File;
import java.util.ArrayList;

import me.rides.Rides.Symbolica.Tours.Track.SymRide;
import me.rides.Rides.Symbolica.Tours.Track.SymTrack;
import me.rides.Rides.Symbolica.Tours.Enums.SymType;
import org.bukkit.scheduler.BukkitRunnable;

import me.rides.core.Core;
import me.rides.interfaces.Manager;
import me.rides.interfaces.Ride;
import me.rides.objects.Segments.Segment;
import me.rides.objects.Segments.SegmentType;

public class SymManager implements Manager {

	private static int speed = 2;
	private SymTrack track1;
	private SymTrack track2;
	private SymTrack track3;
	private SymRide symmuziek;
	private SymRide symhelden;
	private SymRide symschatten;
	
	
	private static ArrayList<Segment> segments1 = new ArrayList<Segment>();
	private static ArrayList<Segment> segments2 = new ArrayList<Segment>();
	private static ArrayList<Segment> segments3 = new ArrayList<Segment>();
	private static boolean vrijgeven;
    private ArrayList<Ride> rides = new ArrayList<Ride>();


    public SymManager() {
        this.setTrack1(new SymTrack(new File(Core.getInstance().getDataFolder(),"/rides/symbolica/muziektour/sym.yml"),429,63,1638));
        this.setTrack2(new SymTrack(new File(Core.getInstance().getDataFolder(),"/rides/symbolica/heldentour/sym.yml"),429,63,1638));
        this.setTrack3(new SymTrack(new File(Core.getInstance().getDataFolder(),"/rides/symbolica/schattentour/sym.yml"),429,63,1638));

        this.segments1.add(new Segment(0, "1-sym-pre", 105, 107, SegmentType.STATION, false));
        this.segments2.add(new Segment(0, "2-sym-pre", 155, 157, SegmentType.STATION, false));
        this.segments3.add(new Segment(0, "3-sym-pre", 207, 209, SegmentType.STATION, false));

        this.segments1.add(new Segment(1, "1-sym-stat", 468, 470, SegmentType.STATION, false));
        this.segments2.add(new Segment(1, "2-sym-stat", 460,462, SegmentType.STATION, false));
        this.segments3.add(new Segment(1, "3-sym-stat", 446,448, SegmentType.STATION, false));
        this.segments1.add(new Segment(2, "1-sym-deur", 806,808, SegmentType.YAW_STOP, 0, 40l, true, false));
	}
	
	public static ArrayList<Segment> getSegments(SymType type) {
		if (type.equals(SymType.MZ)) {
			return segments1;
		}
		if (type.equals(SymType.HD)) {
			return segments2;
		}
		if (type.equals(SymType.SH)) {
			return segments3;
		}
		return null;
	}

	@Override
	public void setup() {
		runtrack();
	}

    @Override
    public ArrayList<Ride> getRides() {
        return this.rides;
    }

    private void runtrack() {
		this.setSymmuziek(new SymRide(this.getTrack1(), SymType.MZ, 100));
		this.setSymhelden(new SymRide(this.getTrack2(), SymType.HD, 50));
		this.setSymschatten(new SymRide(this.getTrack3(), SymType.SH, 0));
		this.rides.add(this.getSymmuziek());
        this.rides.add(this.getSymschatten());
        this.rides.add(this.getSymhelden());
		this.getSymmuziek().start(this.getTrack1(), 100);
		this.getSymhelden().start(this.getTrack2(), 50);
		this.getSymschatten().start(this.getTrack3(), 0);
	}

	public SymTrack getTrack1() {
		return track1;
	}

	public void setTrack1(SymTrack track1) {
		this.track1 = track1;
	}

	public SymTrack getTrack2() {
		return track2;
	}

	public void setTrack2(SymTrack track2) {
		this.track2 = track2;
	}

	public SymTrack getTrack3() {
		return track3;
	}

	public void setTrack3(SymTrack track3) {
		this.track3 = track3;
	}

	public SymRide getSymmuziek() {
		return symmuziek;
	}

	public void setSymmuziek(SymRide symmuziek) {
		this.symmuziek = symmuziek;
	}

	public SymRide getSymhelden() {
		return symhelden;
	}

	public void setSymhelden(SymRide symhelden) {
		this.symhelden = symhelden;
	}

	public SymRide getSymschatten() {
		return symschatten;
	}

	public void setSymschatten(SymRide symschatten) {
		this.symschatten = symschatten;
	}

	public static int getSpeed() {
		return speed;
	}

	public static void setSpeed(int speed) {
		SymManager.speed = speed;
	}

	public void remove() {
		this.getSymhelden().getFantasievaarder().remove();
		this.getSymmuziek().getFantasievaarder().remove();
		this.getSymschatten().getFantasievaarder().remove();
	}

	public static boolean isVrijgegeven() {
		return vrijgeven;
	}

	@Override
	public void setSpeed(Integer valueOf) {
		speed = valueOf;
	}

	@Override
	public void setVrijgeven() {
		vrijgeven = true;
		new BukkitRunnable() {

			@Override
			public void run() {
				vrijgeven = false;
			}
			
		}.runTaskLater(Core.getInstance(), 35);
	}


	
}
