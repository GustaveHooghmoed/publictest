package me.rides.Rides.Symbolica.Tours.Car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import me.rides.objects.TrainsCars.Car2;
import me.rides.objects.TrainsCars.PigStand2;
import me.rides.Rides.Symbolica.Tours.Track.SymTrack;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;

import me.rides.Rides.fatamorgana.Utils.EntityBoatStand;
import me.rides.interfaces.Track;
import me.rides.interfaces.Train;
import me.rides.objects.Locations.Point;
import me.rides.objects.Relative.RelativeUtil;
import me.rides.objects.Relative.StandRelative;
import me.rides.utils.LocationUtil;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.WorldServer;

public class FantasieVaarder implements Train {

	private HashMap<Integer, Car2> stands = new HashMap<>();
	private ArrayList<EntityBoatStand> stands3 = new ArrayList<EntityBoatStand>();
    private ArrayList<EntityArmorStand> stands2 = new ArrayList<EntityArmorStand>();
	private int pos;
	private float yaw;
    
	public FantasieVaarder(SymTrack t, int pos) {
		EntityBoatStand stand = spawnHoofd(t, pos);
		Car2 car = new Car2(getPigStand(stand, t, pos), 0.0d);
		stands.put(pos, car);
		this.yaw = 0.0f;
	}
	public void remove() {
		for(Entry<Integer, Car2> stand : stands.entrySet()) {
			Car2 car = stand.getValue();
			car.getStand().remove();
		}
		for(EntityBoatStand stand : stands3) {
			stand.killEntity();
		}
	}

    @Override
    public String getName() {
        return "sym-fantasievaarder";
    }

    @Override
    public float getYaw() {
		return this.yaw;
    }

    @Override
    public int getPos() {
        return this.pos;
    }

    private EntityBoatStand spawnHoofd(Track t, int pos) {
		Location loc = t.getTracklocations().get(pos).getLoc();
		 EntityBoatStand stand = new EntityBoatStand(loc, (byte)4);
	        stand.setCustomName("fata_head");
	        stand.setNoGravity(true);

	        ((CraftWorld)loc.getWorld()).getHandle().addEntity(stand, SpawnReason.CUSTOM);
	        PacketPlayOutEntityEquipment packet = new PacketPlayOutEntityEquipment(stand.getId(), EnumItemSlot.HEAD,  CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_SWORD,1,(byte)4)));

	        sendPacket(packet);
	        //and so on
	        stands3.add(stand);

	   return stand;
	}

	private void sendPacket(PacketPlayOutEntityEquipment packet) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		}
	}
	private Location setupSeatLoc(Location location, double d, int i, double e) {
		return location.clone().add(e,i,d);
	}
	private EntityArmorStand spawnSeat(Location loc) {
		 WorldServer s = ((CraftWorld)loc.getWorld()).getHandle();
		 EntityArmorStand stand = new EntityArmorStand(s);
	        stand.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
	        stand.setCustomName("python_seat");
	        stand.setNoGravity(true);
	        stand.noclip = true;
	        stands2.add(stand);
	        //and so on
	        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
	        sendPacket(packet);
	        return stand;
	}
	
	private void sendPacket(PacketPlayOutSpawnEntityLiving packet) {
		for(Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}
	private PigStand2 getPigStand(EntityBoatStand stand, Track t, int i) {
		Location location = t.getTracklocations().get(i).loc();
		Location location1 = t.getTracklocations().get(i + 10).loc();
		Location seat1 = setupSeatLoc(location,0.0,0,0.3).add(0.0, 0.6, 0.7);
		Location seat2 = setupSeatLoc(location,0.0,0,-0.3).add(0.0, 0.6, 0.7);;
		Location seat3 = setupSeatLoc(location1,0.0,0,0.3).add(0, 0, 0.1);
		Location seat4 = setupSeatLoc(location1,0.0,0,-0.3).add(0, 0, 0.1);
		PigStand2 pigStand = new PigStand2(spawnSeat(seat1), stand, spawnSeat(seat2), spawnSeat(seat3),spawnSeat(seat4));
		return pigStand;
	}
	
	public void move(int i, Track t) {
		this.pos = i;
		for (Entry<Integer, Car2> as: stands.entrySet()) {
			int pos = i;
			Point loc = t.getTracklocations().get(pos + as.getKey()).toPoint();
			Point loc2 = t.getTracklocations().get(pos + 60 + as.getKey()).toPoint();
			double pitch = LocationUtil.getPitchTo(loc.loc(), loc2.loc());
			Location loca = loc.loc();
			loca.setYaw(LocationUtil.getAngle(loc.loc().toVector(), loc2.loc().toVector()));
			loca.setPitch((float)pitch);
			as.getValue().getStand().getStand().teleport(loca);
			PigStand2 stand = as.getValue().getStand();
			for(StandRelative relative : stand.getRelatives()) {
				RelativeUtil.updateRelative(relative, -180d);
			}
		}
	}

    @Override
    public void moveWithYaw(int i, Track t, double yaw) {
		this.pos = i;
		for (Entry<Integer, Car2> as: stands.entrySet()) {
			int pos = i;
			Point loc = t.getTracklocations().get(pos + as.getKey()).toPoint();
			Point loc2 = t.getTracklocations().get(pos + 60 + as.getKey()).toPoint();
			double pitch = LocationUtil.getPitchTo(loc.loc(), loc2.loc());
			Location loca = loc.loc();
			loca.setYaw((float) yaw);
			loca.setPitch((float)pitch);
			as.getValue().getStand().getStand().teleport(loca);
			PigStand2 stand = as.getValue().getStand();
			for(StandRelative relative : stand.getRelatives()) {
				RelativeUtil.updateRelative(relative,-180d);
			}
		}
    }
	@Override
	public ArrayList<EntityArmorStand> geEntityArmorStands() {
		return stands2;
	}
}
