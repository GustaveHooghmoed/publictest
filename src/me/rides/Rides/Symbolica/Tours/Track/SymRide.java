package me.rides.Rides.Symbolica.Tours.Track;

import me.rides.Rides.Symbolica.Tours.Car.FantasieVaarder;
import me.rides.Rides.Symbolica.Manager.SymManager;
import me.rides.Rides.Symbolica.Tours.Enums.SymType;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.rides.core.Core;
import me.rides.Rides.fatamorgana.Utils.TimeWatch;
import me.rides.interfaces.Ride;
import me.rides.interfaces.Track;
import me.rides.interfaces.Train;
import me.rides.objects.Locations.TrackLocation;
import me.rides.objects.Segments.Segment;
import me.rides.objects.Segments.SegmentType;
import me.rides.utils.LocationUtil;
import me.rides.utils.SegmentUtil;

public class SymRide implements Ride {

	private SymTrack track;
	private SymType type;
	private FantasieVaarder fantasievaarder;

	public SymRide(SymTrack t, SymType mz, int offset) {
		this.setTrack(t);
		this.setType(mz);
		this.setFantasievaarder(new FantasieVaarder(this.track, offset));
	}

	public SymTrack getTrack() {
		return track;
	}

	public void setTrack(SymTrack track) {
		this.track = track;
	}

	public SymType getType() {
		return type;
	}

	public void setType(SymType type) {
		this.type = type;
	}

	public FantasieVaarder getFantasievaarder() {
		return fantasievaarder;
	}

	public void setFantasievaarder(FantasieVaarder fantasievaarder) {
		this.fantasievaarder = fantasievaarder;
	}
	
	public void start(Track t, int start) {
		 TimeWatch watch = TimeWatch.start();
		new BukkitRunnable() {

			private int count = start;
			private int speedseg  = 2;
			private double yaw;
			private double yawstr;
			private boolean yawenabled = true;
			private boolean testvar;
			private boolean resetyaw;
			private boolean resett;

			// SPEED = 4 (AFDALING)
			// SPEED = 1 (OPTAKELING)
			// SPEED = 1 (BRAKE)
			@Override
			public void run() {
				if (this.count < t.getTracklocations().size() - 2) {
					TrackLocation trackLocation = t.getTracklocations().get(count);
					for (Segment segment : SymManager.getSegments(type)) {
						if (count > segment.getPos() - 2 && count < segment.getEndPos() + 2) {
							// ORGINAL SEGMENTS
							if (segment.getId() == 0) {
								if (SegmentUtil.checkFree(SymManager.getSegments(type), 1)) {
									speedseg = 2;
									segment.setBezet(false);
								} else {
									speedseg = 0;
									segment.setBezet(true);
								}
							}
							if (segment.getId() == 1) {
									if (SymManager.isVrijgegeven()) {

										speedseg = 2;
										segment.setBezet(false);
									} else {
										speedseg = 0;
										segment.setBezet(true);
									}
							}
							
							// YAW AND ETC
							if(segment.getType() == SegmentType.YAW_STOP) {
								speedseg = 0;
								if(testvar == false) {
									testvar = true;
									Bukkit.broadcastMessage("fired");
									new BukkitRunnable() {
										public void run() {
											new BukkitRunnable() {
												public void run() {
													if(yawstr < 60d) {
														yaw += 4;
														yawstr += 1d;
														yawenabled = false;
													} else {
														cancel();
														yawstr = 0.0d;
														yawenabled = true;
														resetyaw = true;
													}
												}
											}.runTaskTimer(Core.getInstance(), 0, 0);
										}
									}.runTaskLater(Core.getInstance(), 1l);
								}
								
								new BukkitRunnable() {
									boolean started;
									@Override
									public void run() {
										if(!started) {
											started = true;
											speedseg = 2;
										}
	
									}
									
								}.runTaskLater(Core.getInstance(), segment.getDelay());
								
								new BukkitRunnable() {
									public void run() {
										testvar = false;
									}
								}.runTaskLater(Core.getInstance(), segment.getDelay() + 20);
							}
							
						}
				}
					count += speedseg;
					double officele = LocationUtil.getAngle(t.getTracklocations().get(count).loc().toVector(), t.getTracklocations().get(count + 60).loc().toVector());
					if(yawenabled == false) {
						
					    
					    if(equalsYU(officele, yaw)) {
					    		if(yaw > officele) {
							    	yaw--;
							    } else if(yaw < officele) {
							    	yaw++;
							    } else {
							    	yaw = officele;
							    }
							resett = true;
							

							Bukkit.broadcastMessage("reset yaw");
						}
							
					} else {
						resett = false;
					    	yaw = officele;
							resetyaw = false;
					}
							getFantasievaarder().moveWithYaw(count, track, yaw);
				} else {
					this.count = 0;
					watch.reset();
					getFantasievaarder().move(count, track);
				}
			}
		}.runTaskTimerAsynchronously(Core.getInstance(), 0l, 0l);
	}

    @Override
    public Train getTrain() {
        return fantasievaarder;
    }
    
    final static double EPSILON = 0.1;
    /** Compare two doubles, using default epsilon */
    public static boolean equalsYU(double officele, double current) {
       if(current > officele || current == officele || current + 3 > officele) {
    	   return true;
       }
	return false;
    }

	@Override
	public TrackLocation getClosets(Player p) {
		return track.getClosets(p);
	}
}
