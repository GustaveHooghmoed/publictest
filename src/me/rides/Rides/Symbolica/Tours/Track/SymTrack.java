package me.rides.Rides.Symbolica.Tours.Track;

import java.io.File;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import me.rides.interfaces.Track;
import me.rides.objects.Locations.Point;
import me.rides.objects.Locations.TrackLocation;
import me.rides.utils.StringUtil;

public class SymTrack implements Track {

	private ArrayList<TrackLocation> locsf = new ArrayList<TrackLocation>();
	private ArrayList<TrackLocation> locsfinal = new ArrayList<TrackLocation>();
	private int index;

	public SymTrack(File z,double a, double b, double c) {
		Point centera = new Point(a, b, c);
		Location center = centera.loc();
		try {
			File f = z;
			ArrayList<String> vectors = (ArrayList) FileUtils.readLines(f, "utf-8");
			ArrayList<Point> points = new ArrayList();
			for (String vector : vectors) {
				points.add(StringUtil.getPointFromStringVector1(vector));
			}
			for (Point p : points) {
				TrackLocation loc = new TrackLocation(
						index, center.clone().add(p.getX() + 0.6, (p.getY() + 0.05), p.getZ() + 0.5), p.getYaw());
				loc.setSpeed(5);
				index += 1;
				locsf.add(loc);

			}
			int ind = 0;
			for(TrackLocation location : this.locsf ) {
				location.setIndex(ind);
				locsfinal.add(location);
				ind += 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public ArrayList<TrackLocation> getTracklocations() {
		return locsfinal;
	}
	
	public TrackLocation getClosets(Player p) {
		for (TrackLocation trackLocation : locsfinal) {
			if (trackLocation.getLoc().distance(p.getLocation()) <= 0.6) {
				return trackLocation;
			}
		}
		return locsfinal.get(0);
	}


	@Override
	public void setup(double a, double b, double c) {
		// TODO Auto-generated method stub
		
	}

}
