package me.rides.Rides.fatamorgana.Ride;

import me.rides.Rides.fatamorgana.Car.FataBoot;
import me.rides.Rides.fatamorgana.Manager.FataManager;
import me.rides.Rides.fatamorgana.Utils.TimeWatch;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.rides.core.Core;
import me.rides.interfaces.Ride;
import me.rides.interfaces.Track;
import me.rides.interfaces.Train;
import me.rides.objects.Locations.TrackLocation;

public class FataRide implements Ride {

	private FataTrack track;
	private Train boot1;
	private double yaw;

	public FataRide(String string, FataTrack track, int start) {
		this.setTrack(track);
		this.setBoot1(new FataBoot(string,this.track, start));
	}

	public FataRide(FataTrack track, Train fataBoot, double yaw) {
		this.setTrack(track);
		this.setBoot1(fataBoot);
		this.yaw = yaw;
	}

	public FataTrack getTrack() {
		return track;
	}

	public void setTrack(FataTrack track) {
		this.track = track;
	}

	public void setBoot1(Train boot1) {
		this.boot1 = boot1;
	}
	
	public void start(Track t, int start) {
		 TimeWatch watch = TimeWatch.start();
		new BukkitRunnable() {

			private int count = start;
			private int speed = 15;
			private int speedtrack = 1;
			private int speedseg = 1;

			// SPEED = 4 (AFDALING)
			// SPEED = 1 (OPTAKELING)
			// SPEED = 1 (BRAKE)
			@Override
			public void run() {
				if (this.count < t.getTracklocations().size() - 2) {
					TrackLocation trackLocation = t.getTracklocations().get(count);
//					for (Segment segment : FataManager.getSegments()) {
//						if (count > segment.getPos() - 10 && count < segment.getEndPos() + 10) {
//							if (segment.getSegment_name().equalsIgnoreCase("python-brakes")) {
//								if (SegmentUtil.checkFree(FataManager.getSegments(), "python-station")) {
//									speedseg = 1;
//									segment.setBezet(false);
//								} else {
//									speedseg = 0;
//									segment.setBezet(true);
//								}
//							}
//							if (segment.getSegment_name().equalsIgnoreCase("python-station")) {
//								if (SegmentUtil.checkFree(FataManager.getSegments(), "python-lift")) {
//
//									if (PythonManager.isVrijgegeven()) {
//
//										speedseg = 4;
//										segment.setBezet(false);
//									} else {
//										speedseg = 0;
//										segment.setBezet(true);
//									}
//								} else {
//									speedseg = 2;
//									segment.setBezet(true);
//								}
//							}
//						} else {
							count += FataManager.getSpeed();
						//}
				// }

					getTrain().moveWithYaw(count, track, yaw);
				} else {
					this.count = 0;
					watch.reset();
					getTrain().moveWithYaw(count, track, -200.94647);
				}
			}
		}.runTaskTimerAsynchronously(Core.getInstance(), 0l, 0l);
	}

    @Override
    public Train getTrain() {
        return boot1;
    }

	@Override
	public TrackLocation getClosets(Player p) {
		return track.getClosets(p);
	}
}
