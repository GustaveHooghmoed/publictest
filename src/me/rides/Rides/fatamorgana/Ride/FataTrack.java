package me.rides.Rides.fatamorgana.Ride;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import me.rides.core.Core;
import me.rides.interfaces.Track;
import me.rides.objects.Locations.Point;
import me.rides.objects.Locations.TrackLocation;
import me.rides.utils.BlockFaceUtil;
import me.rides.utils.StringUtil;

public class FataTrack implements Track {

	private int index;
	private ArrayList<TrackLocation> locsf = new ArrayList<TrackLocation>();
	private ArrayList<TrackLocation> locsfinal = new ArrayList<TrackLocation>();

	public FataTrack(double a, double b, double c) {
		Point centera = new Point(a, b, c);
		Location center = centera.loc();
		try {
			File f = new File(Core.getInstance().getDataFolder(), "fata.yml");
			ArrayList<String> vectors = (ArrayList) FileUtils.readLines(f, "utf-8");
			ArrayList<Point> points = new ArrayList();
			for (String vector : vectors) {
				points.add(StringUtil.getPointFromStringVector1(vector));
			}
			int index = 0;
			for (Point p : points) {
				TrackLocation loc = new TrackLocation(
						index, center.clone().add(p.getX() + 0.6, (p.getY() + 0.05) - 1, p.getZ() + 0.5), p.getYaw());
				loc.setSpeed(5);
				locsf.add(loc);
				index += 1;
			}
			for(TrackLocation location : this.locsf ) {
				locsfinal.add(location);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public TrackLocation getClosets(Player p) {
		for (TrackLocation trackLocation : locsfinal) {
			if (trackLocation.getLoc().distance(p.getLocation()) <= 1) {
				return trackLocation;
			}
		}
		return locsfinal.get(0);
	}


	@Override
	public ArrayList<TrackLocation> getTracklocations() {
		return locsfinal;
	}
	
	public static void setupsd(double a,double b, double c, String string, byte d) {
		Point start = new Point(a,b,c);
		Location loc = start.loc();
		List<Material> mat = new ArrayList<>();
	mat.add(Material.EMERALD_BLOCK);
	Thread blockFaceUtil = new BlockFaceUtil(loc.getBlock());
	blockFaceUtil.run();
	List<Block> list = null;
	while(blockFaceUtil.isAlive()) {
		if(!blockFaceUtil.isAlive()) {
			break;
		}
	}
	list = BlockFaceUtil.list;
	Bukkit.broadcastMessage(list.get(0).getType().toString());
	ArrayList<Location> b1l = new ArrayList<Location>();
	for (Block aw : list) {
		if (aw.getData() == d) {
			b1l.add(aw.getLocation());
		}
	}
	File f = new File(Core.getInstance().getDataFolder() + "/test2" + string +"txt");
	try {
		PrintWriter pi = new PrintWriter(f);
		pi.write("coords = [");
		for (Location loctemp : b1l) {
			double x = loctemp.getX() - start.getX();
			double y = loctemp.getY() - start.getY();
			double z = loctemp.getZ() - start.getZ();
			String k = ",";
			pi.write("(" + x + k + y + k + z + "),\n");
		}
		pi.write("]");
		pi.close();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


	@Override
	public void setup(double a, double b, double c) {
		// TODO Auto-generated method stub
		
	}

}
