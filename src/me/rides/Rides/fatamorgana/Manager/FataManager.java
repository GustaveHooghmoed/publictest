package me.rides.Rides.fatamorgana.Manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.rides.Rides.fatamorgana.Utils.BoatConfig;
import me.rides.Rides.fatamorgana.Ride.FataRide;
import me.rides.Rides.fatamorgana.Ride.FataTrack;
import me.rides.Rides.fatamorgana.Utils.TimeWatch;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import me.rides.core.Core;
import me.rides.interfaces.Manager;
import me.rides.interfaces.Ride;
import me.rides.interfaces.Track;
import me.rides.objects.Segments.Segment;

public class FataManager implements Manager {

	private FataTrack track;
	private ArrayList<Ride> rides = new ArrayList<Ride>();
	protected int trackcounter = 0;
	private static ArrayList<Segment> segments = new ArrayList<Segment>();
	private static int speed = 20;

	public FataManager() {
		this.setTrack(new FataTrack(100,63.5,2074));
	}

	@Override
	public void setup() {
		TimeWatch timeWatch = TimeWatch.start();
		for(int i = 0; i < 2 * 14; i+= 2) {
			new BukkitRunnable() {
				
				@Override
				public void run() {
					FataRide fataRide2 = new FataRide("fata-boot-" + trackcounter,getTrack(), 0);
					rides.add(fataRide2);
					fataRide2.start(getTrack(), 0);
					trackcounter += 1;
				}
			}.runTaskLater(Core.getInstance(), 20 * i);
		}
	}

	public static ArrayList<Segment> getSegments() {
		return segments;
	}

	public FataTrack getTrack() {
		return track;
	}

	public void setTrack(FataTrack track) {
		this.track = track;
	}

	@Override
	public void remove() {
		for(Ride ride : rides) {
			ride.getTrain().remove();
		}
	}

	@Override
	public ArrayList<Ride> getRides() {
		return rides;
	}

	public static int getSpeed() {
		return speed;
	}


	@Override
	public void setSpeed(Integer valueOf) {
		speed = valueOf;
	}

	@Override
	public void setVrijgeven() {
		
	}

}
