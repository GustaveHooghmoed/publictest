package me.rides.Rides.fatamorgana.Car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import me.rides.Rides.fatamorgana.Utils.EntityBoatStand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;

import me.rides.interfaces.Track;
import me.rides.interfaces.Train;
import me.rides.objects.TrainsCars.Car;
import me.rides.objects.TrainsCars.PigStand;
import me.rides.objects.Locations.Point;
import me.rides.objects.Relative.RelativeUtil;
import me.rides.objects.Relative.StandRelative;
import me.rides.utils.LocationUtil;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.WorldServer;


public class FataBoot implements Train {
	
	private HashMap<Integer, Car> stands = new HashMap<Integer, Car>();
	private ArrayList<EntityBoatStand> stands3 = new ArrayList<EntityBoatStand>();
    private ArrayList<EntityArmorStand> stands2 = new ArrayList<EntityArmorStand>();
	private String name;
	private int pos;
	private float yaw;

	public FataBoot(String string, Track t, int pos) {
		this.name = string;
		EntityBoatStand stand = spawnHoofd(t, pos);
		stand.setCustomName(string);
		stand.setCustomNameVisible(true);
		Car car = new Car(getPigStand(stand, t, pos), 0.0d);
		stands.put(pos, car);
	}
	
	public FataBoot(String name, Track track, Integer valueOf, double double1) {
		EntityBoatStand stand = spawnHoofd(track, valueOf);
		Location to = stand.getLocation();
		to.setYaw((float)double1);
		
		stand.teleport(to);
		stand.setCustomName(name);
		stand.setCustomNameVisible(true);
		Car car = new Car(getPigStand(stand, track, valueOf), 0.0d);
		stands.put(valueOf, car);
	}

	@Override
	public void remove() {
		for(Entry<Integer, Car> stand : stands.entrySet()) {
			Car car = stand.getValue();
			car.getStand().remove();
		}
		for(EntityBoatStand stand : stands3) {
			stand.killEntity();
		}
	}

	private EntityBoatStand spawnHoofd(Track t, int pos) {
		Location loc = t.getTracklocations().get(pos).getLoc();
		 EntityBoatStand stand = new EntityBoatStand(loc, (byte)1);
	        stand.setCustomName("fata_head");
	        stand.setNoGravity(true);
	        stand.setEquipment(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.AIR,1)));
	        ((CraftWorld)loc.getWorld()).getHandle().addEntity(stand, SpawnReason.CUSTOM);
	        //and so on
	        stands3.add(stand);

	   return stand;
	}


	private PigStand getPigStand(EntityBoatStand stand, Track t, int i) {
		Location location = t.getTracklocations().get(i).loc();
		Location location1 = t.getTracklocations().get(i + 40).loc();
		Location location2 = t.getTracklocations().get((i + 40) + 40).loc();
		Location location3 = t.getTracklocations().get(((i + 40) + 40) + 40).loc();
		Location seat1 = setupSeatLoc(location,0.0,0,0.7);
		Location seat2 = setupSeatLoc(location,0.0,0,-0.7);
		Location seat3 = setupSeatLoc(location1,0.0,0,0.7);
		Location seat4 = setupSeatLoc(location1,0.0,0,-0.7);
		PigStand pigStand = new PigStand(spawnSeat(seat1), stand, spawnSeat(seat2), spawnSeat(seat3),spawnSeat(seat4));
		return pigStand;
	}


	@Override
	public void move(int i, Track t) {
		this.pos = i;
		for (Entry<Integer, Car> as: stands.entrySet()) {
			int pos = i;
			Point loc = t.getTracklocations().get(pos + as.getKey()).toPoint();
			Point loc2 = t.getTracklocations().get(pos + 60 + as.getKey()).toPoint();
			double pitch = LocationUtil.getPitchTo(loc.loc(), loc2.loc());
			Location loca = loc.loc();
			loca.setYaw(LocationUtil.getAngle(loc.loc().toVector(), loc2.loc().toVector()));
			this.yaw = loca.getYaw();
			loca.setPitch((float)pitch);
			as.getValue().getStand().getStand().teleport(loca);
			PigStand stand = as.getValue().getStand();
			for(StandRelative relative : stand.getRelatives()) {
				RelativeUtil.updateRelative(relative,-200.94647);
			}
		}
	}

    @Override
        public void moveWithYaw(int i, Track t, double yaw) {
            this.pos = i;
            for (Entry<Integer, Car> as: stands.entrySet()) {
                int pos = i;
                Point loc = t.getTracklocations().get(pos + as.getKey()).toPoint();
                Point loc2 = t.getTracklocations().get(pos + 60 + as.getKey()).toPoint();
                double pitch = LocationUtil.getPitchTo(loc.loc(), loc2.loc());
                Location loca = loc.loc();
                loca.setYaw(LocationUtil.getAngle(loc.loc().toVector(), loc2.loc().toVector()));
                this.yaw = loca.getYaw();
                loca.setPitch((float) pitch);
                as.getValue().getStand().getStand().teleport(loca);
                PigStand stand = as.getValue().getStand();
                for (StandRelative relative : stand.getRelatives()) {
                    RelativeUtil.updateRelative(relative, yaw);
                }
            }
    }

    public void move2(int i, Track t, double yaw) {
		this.pos = i;
		for (Entry<Integer, Car> as: stands.entrySet()) {
			int pos = i;
			Point loc = t.getTracklocations().get(pos + as.getKey()).toPoint();
			Point loc2 = t.getTracklocations().get(pos + 60 + as.getKey()).toPoint();
			double pitch = LocationUtil.getPitchTo(loc.loc(), loc2.loc());
			Location loca = loc.loc();
			loca.setYaw(LocationUtil.getAngle(loc.loc().toVector(), loc2.loc().toVector()));
			this.yaw = loca.getYaw();
			loca.setPitch((float)pitch);
			as.getValue().getStand().getStand().teleport(loca);
			PigStand stand = as.getValue().getStand();
			for(StandRelative relative : stand.getRelatives()) {
				RelativeUtil.updateRelative(relative,yaw);
			}
		}
	}

	private Location setupSeatLoc(Location location, double d, int i, double e) {
		return location.clone().add(e,i,d);
	}
	private EntityArmorStand spawnSeat(Location loc) {
		 WorldServer s = ((CraftWorld)loc.getWorld()).getHandle();
		 EntityArmorStand stand = new EntityArmorStand(s);
	        stand.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
	        stand.setCustomName("python_seat");
	        stand.setNoGravity(true);
	        stand.noclip = true;
	        stands2.add(stand);
	        //and so on
	        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
	        sendPacket(packet);
	        return stand;
	}

	private void sendPacket(PacketPlayOutSpawnEntityLiving packet) {
		for(Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public ArrayList<EntityArmorStand> geEntityArmorStands() {
		return stands2;
	}

	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public int getPos() {
		return this.pos;
	}
	@Override
	public float getYaw() {
		return this.yaw;
	}
}
