package me.rides.Rides.fatamorgana.Utils;

import me.rides.Rides.fatamorgana.Car.FataBoot;
import me.rides.interfaces.Track;

public class BoatConfig {

	private Track track;
	private String pos;
	private String yaw;
	private String name;

	public BoatConfig(String name, Track t, String position, String yaw) {
		this.name = name;
		this.setTrack(t);
		this.setPos(position);
		this.setYaw(yaw);
	}

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getYaw() {
		return yaw;
	}

	public void setYaw(String yaw) {
		this.yaw = yaw;
	}
	
	public FataBoot toBoot() {
		return new FataBoot(name, track, Integer.valueOf(pos),Double.valueOf(yaw));
		
	}

}
