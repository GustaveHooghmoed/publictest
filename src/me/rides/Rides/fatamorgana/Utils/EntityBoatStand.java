package me.rides.Rides.fatamorgana.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import me.rides.core.Core;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;

public class EntityBoatStand extends EntityArmorStand {
	
	public EntityBoatStand(Location loc, byte b) {
	        super(((CraftWorld)loc.getWorld()).getHandle());
	        this.setPosition(loc.getX(), loc.getY(), loc.getZ());
	        this.noclip = true;
	        this.setEquipment(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(Material.DIAMOND_SWORD,1,(byte)4)));
	 }

	public Location getLocation() {
		return this.getBukkitEntity().getLocation();
	}

	public void teleport(Location to) {
		this.setPositionRotation(to.getX(), to.getY(),to.getZ(), to.getYaw(), to.getPitch());
	}
	
	
	 
}
