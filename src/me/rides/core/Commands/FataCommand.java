package me.rides.core.Commands;

import java.util.ArrayList;

import me.rides.core.Core;
import me.rides.interfaces.Manager;
import me.rides.interfaces.Ride;
import me.rides.objects.Packets.WrapperPlayClientUseEntity;
import me.rides.objects.Packets.WrapperPlayServerMount;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import me.rides.Rides.fatamorgana.Manager.FataManager;
import me.rides.Rides.fatamorgana.Ride.FataRide;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutMount;

public class FataCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] args) {
		Manager fataManager = Core.fManager;
		if(args[0].equalsIgnoreCase("setup")) {
			fataManager.setup();
			addListeners();
		}
		if(args[0].equalsIgnoreCase("speed")) {
			fataManager.setSpeed(Integer.valueOf(args[1]));
		}
		return false;
	}

	public static void addListeners() {
		new BukkitRunnable() {
			 @Override
			    public void run() {
			        for (Player player : Bukkit.getOnlinePlayers()) {
			            for (Entity entity : Core.getConfiguration().getWorld().getEntities()) {
			                if (entity.getPassenger() != null) {
			                    PacketPlayOutMount packetPlayOutMount = new PacketPlayOutMount(((CraftEntity) entity).getHandle());
			                    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutMount);
			                }
			            }
			        }
			    }
		}.runTaskTimer(Core.getInstance(), 1l, 1l);
				ProtocolLibrary.getProtocolManager().addPacketListener(
		                new PacketAdapter(Core.getInstance(), WrapperPlayClientUseEntity.TYPE) {
		                    @Override
		                    public void onPacketReceiving(PacketEvent event) {
		                        if (event.getPacketType() == WrapperPlayClientUseEntity.TYPE) {
		                            WrapperPlayClientUseEntity packet = new WrapperPlayClientUseEntity(event.getPacket());
		                            int entityID = packet.getTargetID();
		                           
		                            for(Ride blockTestSM : Core.fManager.getRides()) {
		                            	 for(EntityArmorStand entityArmorStand : blockTestSM.getTrain().geEntityArmorStands()) {
		                                 	
		                                 	if(entityArmorStand.getId() == entityID) {
		                                 		Bukkit.broadcastMessage("mounting " + event.getPlayer().getName()) ;
		                                 		WrapperPlayServerMount wrapped = new WrapperPlayServerMount();
		                                 		wrapped.setEntityID(entityID);
		                                 		ArrayList<Entity> list = new 	ArrayList<Entity>();
		                                 		list.add(((CraftPlayer)event.getPlayer()).getHandle().getBukkitEntity());
		                                 		wrapped.setPassengers(list);
		                                 		for(Player player :Bukkit.getOnlinePlayers()) {
		                                 			wrapped.sendPacket(player);
		                                 		}
		                        
		                                 	}
		                                 }
		                            }
		                           
		                            }
		                        }
		                    }

		                );
			}
			public static void reloadPacket() {
				for(Player player : Bukkit.getOnlinePlayers()) {
					PacketPlayOutMount packet = new PacketPlayOutMount(((CraftPlayer) player).getHandle());
					for(Player players : Bukkit.getOnlinePlayers()) {
						((CraftPlayer)players).getHandle().playerConnection.sendPacket(packet);
					}
				}
			}
}
