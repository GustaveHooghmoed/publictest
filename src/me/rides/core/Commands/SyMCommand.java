package me.rides.core.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.rides.core.Core;
import me.rides.interfaces.Manager;

public class SyMCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] args) {
		Manager fataManager = Core.sManager;
		if(args[0].equalsIgnoreCase("setup")) {
			fataManager.setup();
		}
		if(args[0].equalsIgnoreCase("vrij")) {
			fataManager.setVrijgeven();
		}
		if(args[0].equalsIgnoreCase("cl1")) {
			Player p = (Player) arg0;
			p.sendMessage(("cl1 "  + Core.sManager.getRides().get(0).getClosets(p).getid()));
		}
		if(args[0].equalsIgnoreCase("cl2")) {
			Player p = (Player) arg0;
			p.sendMessage(("cl2 "  + Core.sManager.getRides().get(1).getClosets(p).getid()));
		}
		if(args[0].equalsIgnoreCase("cl3")) {
			Player p = (Player) arg0;
			p.sendMessage(("cl3 "  + Core.sManager.getRides().get(2).getClosets(p).getid()));
		}
		return false;
	}

}
