package me.rides.core;

import me.rides.core.Commands.FataCommand;
import me.rides.core.Commands.SyMCommand;
import me.rides.interfaces.Manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;

import me.rides.Rides.fatamorgana.Manager.FataManager;
import me.rides.Rides.Symbolica.Manager.SymManager;
import me.rides.utils.Broadcaster;

public class Core extends JavaPlugin implements Listener {
	
	public static Plugin pl = null;
	public static Configuration configuration;
	public static Manager fManager;
	public static Manager sManager;
	
	@Override
	public void onEnable() {
		try {
		    pl = this;
		    configuration = new Configuration(this);
		    Bukkit.getPluginManager().registerEvents(this, this);
		    getCommand("fata").setExecutor(new FataCommand());
		    getCommand("sym").setExecutor(new SyMCommand());
			fManager = new FataManager();
			sManager = new SymManager();
			new Broadcaster().castWithPerm(ChatColor.AQUA + "Core" + ChatColor.GRAY + " > " + ChatColor.WHITE + "is aan het opstarten.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@EventHandler
	public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
		if (event.getRightClicked().getType() == EntityType.ARMOR_STAND) {
			if (event.getRightClicked().getCustomName().equalsIgnoreCase("python_head")) {
				event.setCancelled(true);
			}
			if (event.getRightClicked().getCustomName().equalsIgnoreCase("python_seat")) {
				event.setCancelled(true);
				event.getRightClicked().setPassenger(event.getPlayer());
			}
		}
	}
	

	
	@Override
	public void onDisable() {
		sManager.remove();
		fManager.remove();
		ProtocolLibrary.getProtocolManager().removePacketListeners(Core.getInstance());
		pl = null;
	}

	public static Plugin getInstance() {
		return pl;
	}

	public static Configuration getConfiguration() {
		return configuration;
	}
	
}
