package me.rides.core;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import com.earth2me.essentials.Essentials;

public class Configuration {
	
	private Essentials essentials;
	private Plugin plugin;
	private World world;

	public Configuration(Plugin plugin) {
		this.setPlugin(plugin);
		this.setEssentials((Essentials) Bukkit.getPluginManager().getPlugin("Essentials"));
		this.setWorld(Bukkit.getWorld("park"));
	}

	public Plugin getPlugin() {
		return this.plugin;
	}

	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}

	public Essentials getEssentials() {
		return essentials;
	}

	public void setEssentials(Essentials essentials) {
		this.essentials = essentials;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

}
