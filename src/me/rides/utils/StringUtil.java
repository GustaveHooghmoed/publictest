/**
 * 
 */
package me.rides.utils;

import me.rides.objects.Locations.Point;

/**
 * @author Michel
 *
 */
public class StringUtil {

	public static Point getPointFromStringVector(String vector)
	/*     */ {
		/* 85 */ double x = 0.0D;
		/* 86 */ double y = 0.0D;
		/* 87 */ double z = 0.0D;
		/*     */
		/* 89 */ String rem = vector.replace("v ", "");
		/* 90 */ String[] args = rem.split(" ");
		/*     */
		/* 92 */ x = Double.parseDouble(args[0]);
		/* 93 */ y = Double.parseDouble(args[1]);
		/* 94 */ z = Double.parseDouble(args[2]);

		/*     */
		/* 96 */ return new Point(x, y, z);
		/*     */ }

	public static Point getPointFromStringVector1(String vector)
	/*     */ {
		/* 85 */ double x = 0.0D;
		/* 86 */ double y = 0.0D;
		/* 87 */ double z = 0.0D;
		double a = 0.0D;
		/*     */
		/* 89 */ String rem = vector.replace("v ", "");
		/* 90 */ String[] args = rem.split(" ");
		/*     */
		/* 92 */ x = Double.parseDouble(args[0]);
		/* 93 */ y = Double.parseDouble(args[1]);
		/* 94 */ z = Double.parseDouble(args[2]);
		a = Double.parseDouble(args[3]);
		/*     */
		/* 96 */ return new Point(x, y, z, a);
		/*     */ }

}