package me.rides.utils;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.util.Vector;

import me.rides.core.Core;
import net.minecraft.server.v1_12_R1.EntityLiving;

public class LocationUtil {

	public static ArrayList<Location> getCircleLocs(Location center, double radius, int amount)
	/*     */ {
		/* 88 */ World world = center.getWorld();
		/* 89 */ double increment = 6.283185307179586D / amount;
		/* 90 */ ArrayList<Location> locations = new ArrayList();
		/* 91 */ for (int i = 0; i < amount; i++)
		/*     */ {
			/* 93 */ double angle = i * increment;
			/* 94 */ double x = center.getX() + radius * Math.cos(angle);
			/* 95 */ double z = center.getZ() + radius * Math.sin(angle);
			/* 96 */ locations.add(new Location(world, x, center.getY(), z));
			/*     */ }
		/* 98 */ return locations;
		/*     */ }

	/*     */ public static void setM(org.bukkit.entity.Entity target, float yaw)
	/*     */ {
		/* 313 */ net.minecraft.server.v1_12_R1.Entity cart2 = ((CraftEntity) target).getHandle();
		/* 314 */ cart2.yaw = yaw;
		/* 315 */ if (!(target instanceof EntityLiving)) {
			/* 316 */ return;
			/*     */ }
		/* 318 */ EntityLiving handle = (EntityLiving) target;
		/* 319 */ yaw = clampYaw(yaw);
		/* 320 */ handle.aO = yaw;
		/* 321 */ if (!(handle instanceof net.minecraft.server.v1_12_R1.EntityHuman)) {
			/* 322 */ handle.aM = yaw;
			/*     */ }
		/* 324 */ handle.aP = yaw;
		/*     */ }

	/*     */ public static float getAngle(Vector point1, Vector point2)
	/*     */ {
		/* 48 */ double dx = point2.getX() - point1.getX();
		/* 49 */ double dz = point2.getZ() - point1.getZ();
		/* 50 */ float angle = (float) Math.toDegrees(Math.atan2(dz, dx)) - 90.0F;
		/* 51 */ if (angle < 0.0F) {
			/* 52 */ angle += 360.0F;
			/*     */ }
		/* 54 */ return angle;
		/*     */ }

	public static float clampYaw(float yaw)
	/*     */ {
		/* 302 */ while (yaw < -180.0F) {
			/* 303 */ yaw += 360.0F;
			/*     */ }
		/* 305 */ while (yaw >= 180.0F) {
			/* 306 */ yaw -= 360.0F;
			/*     */ }
		/* 308 */ return yaw;
		/*     */ }

	public static float getPitchTo(Location from, Location to)
	/*     */ {
		/* 353 */ Location fromclone = from.clone();
		/* 354 */ Location toclone = to.clone();
		/* 355 */ fromclone.setDirection(toclone.subtract(fromclone.toVector()).toVector());
		/* 356 */ return fromclone.getPitch();
		/*     */ }

	/*     */ public static Location getRelative(Location center, double distance, double angle) {
		/* 36 */ double yaw = Math.toRadians(angle);
		/*     */
		/* 38 */ double x = center.getX() + distance * Math.cos(yaw);
		/* 39 */ double z = center.getZ() + distance * Math.sin(yaw);
		/*     */
		/* 41 */ Location to = new Location(Core.getConfiguration().getWorld(), x, center.getY(), z);
		/*     */
		/* 43 */ return to;
		/*     */ }

	public static Vector rotateAroundAxisX(Vector v, double cos, double sin) {
		double y = v.getY() * cos - v.getZ() * sin;
		double z = v.getY() * sin + v.getZ() * cos;
		return v.setY(y).setZ(z);
	}

	public static Vector rotateAroundAxisY(Vector v, double cos, double sin) {
		double x = v.getX() * cos + v.getZ() * sin;
		double z = v.getX() * -sin + v.getZ() * cos;
		return v.setX(x).setZ(z);
	}

	public static Vector rotateAroundAxisZ(Vector v, double cos, double sin) {
		double x = v.getX() * cos - v.getY() * sin;
		double y = v.getX() * sin + v.getY() * cos;
		return v.setX(x).setY(y);
	}

}
