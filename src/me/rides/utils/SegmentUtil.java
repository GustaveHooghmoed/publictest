package me.rides.utils;

import java.util.ArrayList;

import me.rides.objects.Segments.Segment;

public class SegmentUtil {
	 public static boolean checkFree(ArrayList<Segment> segments, String s) {
	        boolean free = true;
	        for (Segment segment : segments){
	            if(segment.getSegment_name().equalsIgnoreCase(s)){
	                if(segment.isBezet()){
	                    free = false;
	                } else {
	                    free = true;
	                }
	            }
	        }
	        return free;
	    }

	public static boolean checkFree(ArrayList<Segment> segments, int s) {
	        boolean free = true;
	        for (Segment segment : segments){
	            if(segment.getId() == s){
	                if(segment.isBezet()){
	                    free = false;
	                } else {
	                    free = true;
	                }
	            }
	        }
	        return free;
	    }
}
