package me.rides.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;


public class BlockFaceUtil extends Thread{

	private final BlockFace[] faces = BlockFace.values();
	public static List<Block> list = new ArrayList<Block>();
	public boolean found;
	private Block relaa;
	private Block start;
	private boolean started;

	public BlockFaceUtil(Block start) {
		this.start = start;
		relaa = start;
	}

	// very pseudo code so don't copy pasta, also doesn't take into account the conditions or looping
	Block recursiveMethod(Block someBlock, List<Block> blockList) {
	    for(BlockFace face : faces) {
	    	Block rela = someBlock.getRelative(face);
	    	if(!blockList.contains(rela) && rela.getType() == Material.WOOL) {
				blockList.add(rela);
				relaa = recursiveMethod(rela,blockList);
	    	} else if(blockList.contains(rela) && rela.getX() == start.getX() && rela.getY() == start.getY() && rela.getZ() == start.getZ()) {
	    		this.interrupt();
	    		Bukkit.broadcastMessage("Interrupted");
	    	}
	    }
		return relaa;
	}
	
	@Override
	public void run() {
		if(started == false) {
			started = true;
			relaa = recursiveMethod(relaa,BlockFaceUtil.list);
		}
	}

	public static void reportException(Exception e, Class<?> c) {
		 Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&',
				"&4Unhandled exception caught: &c" + e.getMessage() + " in " + c.getName()));
		 e.printStackTrace();
		}

}
