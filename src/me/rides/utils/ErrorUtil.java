package me.rides.utils;

import org.bukkit.Bukkit;

public class ErrorUtil {
	public static void reportException(Exception e, Class<?> c) {
		Bukkit.broadcast("&4Unhandled exception caught: &c" + e.getMessage() + " in " + c.getName(), "PC.Admin");
		e.printStackTrace();
	}

	public static void reportExceptionTest(String e, Class<?> c) {
		Bukkit.broadcast("&4Unhandled exception caught: &c" + e + " in " + c.getName(), "PC.Admin");
	}
}
