package me.rides.objects.TrainsCars;

import me.rides.Rides.fatamorgana.Utils.EntityBoatStand;
import me.rides.objects.Relative.StandRelative;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class PigStand2 {
    private EntityBoatStand stand;
    private EntityArmorStand pig;
    private EntityArmorStand pig2;
    private EntityArmorStand pig3;
    private EntityArmorStand pig4;
    private StandRelative standrela1;
    private StandRelative standrela2;
    private StandRelative standrela3;
    private StandRelative standrela4;
    private ArrayList<StandRelative> relatives = new  ArrayList<StandRelative>();

    public StandRelative getStandrela1() {
        return standrela1;
    }

    public void setStandrela1(StandRelative standrela1) {
        this.standrela1 = standrela1;
    }

    public StandRelative getStandrela2() {
        return standrela2;
    }

    public void setStandrela2(StandRelative standrela2) {
        this.standrela2 = standrela2;
    }

    public StandRelative getStandrela3() {
        return standrela3;
    }

    public void setStandrela3(StandRelative standrela3) {
        this.standrela3 = standrela3;
    }

    public StandRelative getStandrela4() {
        return standrela4;
    }

    public void setStandrela4(StandRelative standrela4) {
        this.standrela4 = standrela4;
    }

    public PigStand2(EntityArmorStand pig, EntityBoatStand stand, EntityArmorStand pig2, EntityArmorStand pig3, EntityArmorStand pig4) {
        this.pig = pig;
        this.setPig2(pig2);
        this.setPig3(pig3);
        this.setPig4(pig4);
        this.stand = stand;
        this.standrela1 = new StandRelative(this.stand, this.pig);
        this.standrela2 = new StandRelative(this.stand, this.pig2);
        this.standrela3 = new StandRelative(this.stand, this.pig3);
        this.standrela4 = new StandRelative(this.stand, this.pig4);
        relatives.add(standrela1);
        relatives.add(standrela2);
        relatives.add(standrela3);
        relatives.add(standrela4);
    }

    public EntityBoatStand getStand() {
        return stand;
    }

    public void setStand(EntityBoatStand stand) {
        this.stand = stand;
    }

    public EntityArmorStand getPig() {
        return pig;
    }

    public void setPig(EntityArmorStand pig) {
        this.pig = pig;
    }

    public EntityArmorStand getPig2() {
        // TODO Auto-generated method stub
        return pig2;
    }

    public void setPig2(EntityArmorStand pig2) {
        this.pig2 = pig2;
    }

    public EntityArmorStand getPig3() {
        return pig3;
    }

    public void setPig3(EntityArmorStand pig3) {
        this.pig3 = pig3;
    }

    public EntityArmorStand getPig4() {
        return pig4;
    }

    public void setPig4(EntityArmorStand pig4) {
        this.pig4 = pig4;
    }

    public void remove() {
        PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(pig.getId());
        stand.killEntity();
        PacketPlayOutEntityDestroy packet2 = new PacketPlayOutEntityDestroy(pig3.getId());
        PacketPlayOutEntityDestroy packet3 = new PacketPlayOutEntityDestroy(pig2.getId());
        PacketPlayOutEntityDestroy packet4 = new PacketPlayOutEntityDestroy(pig4.getId());

        for(Player player : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet2);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet3);
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet4);
        }
    }

    public ArrayList<StandRelative> getRelatives() {
        return relatives;
    }
}
