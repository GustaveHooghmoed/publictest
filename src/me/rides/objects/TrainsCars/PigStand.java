package me.rides.objects.TrainsCars;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import me.rides.Rides.fatamorgana.Utils.EntityBoatStand;
import me.rides.objects.Relative.StandRelative;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;

public class PigStand {
	private EntityBoatStand stand;
	private EntityArmorStand pig;
	private EntityArmorStand pig2;
	private EntityArmorStand pig3;
	private EntityArmorStand pig4;
	private StandRelative standrela1;
	private StandRelative standrela2;
	private StandRelative standrela3;
	private StandRelative standrela4;
	private ArrayList<StandRelative> relatives = new  ArrayList<StandRelative>();
	private EntityArmorStand pig5;
	private EntityArmorStand pig6;
	private StandRelative standrela5;
	private StandRelative standrela6;
	private EntityArmorStand pig7;
	private EntityArmorStand pig8;
	private StandRelative standrela7;
	private StandRelative standrela8;

	public StandRelative getStandrela1() {
		return standrela1;
	}

	public void setStandrela1(StandRelative standrela1) {
		this.standrela1 = standrela1;
	}

	public StandRelative getStandrela2() {
		return standrela2;
	}

	public void setStandrela2(StandRelative standrela2) {
		this.standrela2 = standrela2;
	}

	public StandRelative getStandrela3() {
		return standrela3;
	}

	public void setStandrela3(StandRelative standrela3) {
		this.standrela3 = standrela3;
	}

	public StandRelative getStandrela4() {
		return standrela4;
	}

	public void setStandrela4(StandRelative standrela4) {
		this.standrela4 = standrela4;
	}

	public PigStand(EntityArmorStand pig, EntityBoatStand stand, EntityArmorStand pig2, EntityArmorStand pig3, EntityArmorStand pig4) {
		this.pig = pig;
		this.setPig2(pig2);
		this.setPig3(pig3);
		this.setPig4(pig4);
		this.stand = stand;
		this.standrela1 = new StandRelative(this.stand, this.pig);
		this.standrela2 = new StandRelative(this.stand, this.pig2);
		this.standrela3 = new StandRelative(this.stand, this.pig3);
		this.standrela4 = new StandRelative(this.stand, this.pig4);
		relatives.add(standrela1);
		relatives.add(standrela2);
		relatives.add(standrela3);
		relatives.add(standrela4);
	}
	
//	public PigStand(EntityArmorStand pig, EntityBoatStand stand2, EntityArmorStand pig2, EntityArmorStand pig3, EntityArmorStand pig4, EntityArmorStand pig5, EntityArmorStand pig6, EntityArmorStand pig7, EntityArmorStand pig8) {
//		this.pig = pig;
//		this.setPig2(pig2);
//		this.setPig3(pig3);
//		this.setPig4(pig4);
//		this.setPig5(pig5);
//		this.setPig6(pig6);
//		this.pig7 = pig7;
//		this.pig8 = pig8;
//		this.stand = stand2;
//		this.standrela1 = new StandRelative(this.stand, this.pig);
//		this.standrela2 = new StandRelative(this.stand, this.pig2);
//		this.standrela3 = new StandRelative(this.stand, this.pig3);
//		this.standrela4 = new StandRelative(this.stand, this.pig4);
//		this.setStandrela5(new StandRelative(this.stand, this.pig5));
//		this.setStandrela6(new StandRelative(this.stand, this.pig6));
//		this.standrela7 = new StandRelative(this.stand, this.pig7);
//		this.standrela8 = new StandRelative(this.stand, this.pig8);
//		relatives.add(standrela1);
//		relatives.add(standrela2);
//		relatives.add(standrela3);
//		relatives.add(standrela4);
//		relatives.add(standrela5);
//		relatives.add(standrela6);
//		relatives.add(standrela7);
//		relatives.add(standrela8);
//	}

	public EntityBoatStand getStand() {
		return stand;
	}

	public void setStand(EntityBoatStand stand) {
		this.stand = stand;
	}

	public EntityArmorStand getPig() {
		return pig;
	}

	public void setPig(EntityArmorStand pig) {
		this.pig = pig;
	}

	public EntityArmorStand getPig2() {
		// TODO Auto-generated method stub
		return pig2;
	}

	public void setPig2(EntityArmorStand pig2) {
		this.pig2 = pig2;
	}

	public EntityArmorStand getPig3() {
		return pig3;
	}

	public void setPig3(EntityArmorStand pig3) {
		this.pig3 = pig3;
	}

	public EntityArmorStand getPig4() {
		return pig4;
	}

	public void setPig4(EntityArmorStand pig4) {
		this.pig4 = pig4;
	}

	public void remove() {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(pig.getId());
		stand.killEntity();
		PacketPlayOutEntityDestroy packet2 = new PacketPlayOutEntityDestroy(pig3.getId());
		PacketPlayOutEntityDestroy packet3 = new PacketPlayOutEntityDestroy(pig2.getId());
		PacketPlayOutEntityDestroy packet4 = new PacketPlayOutEntityDestroy(pig4.getId());
		PacketPlayOutEntityDestroy packet5 = new PacketPlayOutEntityDestroy(pig5.getId());
		PacketPlayOutEntityDestroy packet6 = new PacketPlayOutEntityDestroy(pig6.getId());
		PacketPlayOutEntityDestroy packet7 = new PacketPlayOutEntityDestroy(pig7.getId());
		PacketPlayOutEntityDestroy packet8 = new PacketPlayOutEntityDestroy(pig8.getId());
		for(Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet2);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet3);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet4);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet5);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet6);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet7);
			((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet8);
			}
	}

	public ArrayList<StandRelative> getRelatives() {
		return relatives;
	}

	public EntityArmorStand getPig5() {
		return pig5;
	}

	public void setPig5(EntityArmorStand pig5) {
		this.pig5 = pig5;
	}

	public EntityArmorStand getPig6() {
		return pig6;
	}

	public void setPig6(EntityArmorStand pig6) {
		this.pig6 = pig6;
	}

	public StandRelative getStandrela5() {
		return standrela5;
	}

	public void setStandrela5(StandRelative standrela5) {
		this.standrela5 = standrela5;
	}

	public StandRelative getStandrela6() {
		return standrela6;
	}

	public void setStandrela6(StandRelative standrela6) {
		this.standrela6 = standrela6;
	}
	
	public StandRelative getStandrela7() {
		return standrela7;
	}

	public void setStandrela7(StandRelative standrela6) {
		this.standrela7 = standrela6;
	}
	
	public StandRelative getStandrela8() {
		return standrela8;
	}

	public void setStandrela8(StandRelative standrela6) {
		this.standrela8 = standrela6;
	}
	
	public EntityArmorStand getPig7() {
		return pig7;
	}
	
	public EntityArmorStand getPig8() {
		return pig8;
	}
}
