package me.rides.objects.TrainsCars;

public class Car {

	private PigStand stand;
	private Double d;
	private int pos;

	public Car(PigStand stand, Double d) {
		this.setStand(stand);
		this.setD(0.0d);
	}

	public PigStand getStand() {
		return stand;
	}

	public void setStand(PigStand stand) {
		this.stand = stand;
	}

	public Double getD() {
		return d;
	}

	public void setD(Double d) {
		this.d = d;
	}

	public int getPosNow() {
		// TODO Auto-generated method stub
		return this.pos;
	}

	public void setPosNow(int pos) {
		this.pos = pos;
	}

}
