package me.rides.objects.Relative;

import java.lang.reflect.Field;
/*    */ import java.util.HashMap;

import org.bukkit.Bukkit;
/*    */ import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
/*    */ import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

import me.rides.core.Core;
import me.rides.Rides.fatamorgana.Utils.EntityBoatStand;
import me.rides.utils.LocationUtil;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityTeleport;

/*    */
/*    */
/*    */ public class RelativeUtil
/*    */ {
	public static void moveAS(EntityArmorStand as, Location loca) {
		RelativeUtil.move(as, loca);
	}
	/* 13 */ public static HashMap<ArmorStand, StandRelative> relatives = new HashMap();
	/*    */
	/*    */ public static void updateRelative(StandRelative relative, double extraYaw)
	/*    */ {
		EntityBoatStand centers = relative.getCenter2();
		/* 18 */ EntityArmorStand secondarys = relative.getSecondaryStand();
		
		Location center = centers.getLocation();
		 Location to = LocationUtil.getRelative(center, relative.getDistance(),(
		centers.getLocation().getYaw() + relative.getAngle()) + extraYaw);
	to.setYaw(centers.getLocation().getYaw() );
		moveAS(secondarys, new Location(Core.getConfiguration().getWorld(), to.getX(), relative.getCenter2().getLocation().getY() - relative.getYDifference(), to.getZ(), center.getYaw(), 0.0f));
		/*    */ }
	  private static void setPrivateField(Class type, Object object, String name, Object value)
	  {
	    try
	    {
	      Field f = type.getDeclaredField(name);
	      f.setAccessible(true);
	      f.set(object, value);
	      f.setAccessible(false);
	    }
	    catch (Exception ex)
	    {
	      ex.printStackTrace();
	    }
	  }
	  
	  public static byte getCompressedAngle(float value)
	  {
	      return (byte)(value * 256.0F / 360.0F);
	  }
	  
	  public static byte getCompressedPitchAngle(float value)
	  {
	      return (byte)(value * 256.0F );
	  }
	  
	  
	  
	public static PacketPlayOutEntityTeleport move(EntityArmorStand stand, Location l) {
		PacketPlayOutEntityTeleport tp = new PacketPlayOutEntityTeleport();
		stand.locX = l.getX();
		stand.locY = l.getY();
		stand.locZ = l.getZ();
		stand.yaw = l.getYaw();
		stand.pitch = l.getPitch();
		setPrivateField(PacketPlayOutEntityTeleport.class, tp, "a", Integer.valueOf(stand.getId()));
	    setPrivateField(PacketPlayOutEntityTeleport.class, tp, "b", 
	      l.getX());
	    setPrivateField(PacketPlayOutEntityTeleport.class, tp, "c", 
	      l.getY());
	    setPrivateField(PacketPlayOutEntityTeleport.class, tp, "d", 
	      l.getZ());
	    setPrivateField(PacketPlayOutEntityTeleport.class, tp, "e", 
	      Byte.valueOf(getCompressedAngle(l.getYaw())));
	    setPrivateField(PacketPlayOutEntityTeleport.class, tp, "f", 
	      Byte.valueOf(getCompressedPitchAngle(l.getPitch())));
	    sendPacket(tp);
	    return tp;
	}
	
	public static PacketPlayOutEntityLook newMove(EntityArmorStand armorStand, Location loc) {
		PacketPlayOutEntityLook packet3 = new PacketPlayOutEntityLook(armorStand.getId(), (byte)(loc.getYaw() * 256/ 360), (byte)(loc.getPitch() * 256 / 360), false);
		for(Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet3);
		}

		return packet3;
		
	}
	
	    private static void sendPacket(PacketPlayOutRelEntityMoveLook tp) {
	    	for(Player player : Bukkit.getOnlinePlayers()) {
				((CraftPlayer) player).getHandle().playerConnection.sendPacket(tp);
			}
		
	}
	
	
	public static void sendPacket(PacketPlayOutEntityTeleport packet) {
		for(Player player : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		}
	}
	public static void updateRelative2(StandRelative relative, double extraYaw)
	/*    */ {
		 EntityBoatStand centers = relative.getCenter2();
			/* 18 */ EntityArmorStand secondarys = relative.getSecondaryStand();
			
			Location center = centers.getLocation();
			 Location to = LocationUtil.getRelative(center, relative.getDistance(),(
			centers.getLocation().getYaw() + relative.getAngle()) + extraYaw);
		to.setYaw(centers.getLocation().getYaw() );
		moveAS(secondarys, new Location(Core.getConfiguration().getWorld(), to.getX(), relative.getCenter2().getLocation().getY() - relative.getYDifference(), to.getZ(), center.getYaw(), 0.0f));
		/*    */ }
}

