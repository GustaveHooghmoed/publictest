package me.rides.objects.Locations;

import org.bukkit.Location;

import me.rides.core.Core;

public class Point {

	private double x;
	private double y;
	private double z;
	private double yaw;
	private double pitch;

	public Point(Location location) {
		this.x = location.getX();
		this.y = location.getY();
		this.z = location.getZ();
		this.yaw = location.getYaw();
		this.pitch = 0.0d;
	}

	public Point(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = 0.0d;
		this.pitch = 0.0d;
	}

	public Point(Point pointFromVector, double yawFromVector, double pitch) {
		this.x = pointFromVector.getX();
		this.y = pointFromVector.getY();
		this.z = pointFromVector.getZ();
		this.yaw = yawFromVector;
		this.pitch = pitch;
	}

	public Point(double x2, double y2, double z2, double yaw2) {
		this.x = x2;
		this.y = y2;
		this.z = z2;
		this.yaw = yaw2;
		this.pitch = 0.0d;
	}

	/**
	 * 
	 */
	public Point() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.yaw = 0.0d;
		this.pitch = 0.0d;
	}

	public double getYaw() {
		return this.yaw;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	public void setX(double X) {
		this.x = X;
	}

	public void setY(double Y) {
		this.y = Y;
	}

	public void setZ(double Z) {
		this.z = Z;
	}

	public Point add(Point relative) {
		double a = this.getX();
		double b = this.getY();
		double c = this.getZ();

		a += relative.getX();
		b += relative.getY();
		c += relative.getZ();

		return new Point(a, b, c);
	}

	public void setYaw(double yaw2) {
		this.yaw = yaw2;
	}

	public Point clonep() {
		return new Point(this.x, this.y, this.z);
	}

	public Location loc() {
		return new Location(Core.getConfiguration().getWorld(), x, y, z);
	}

	public Point toRad() {
		double a = Math.toRadians(x);
		double b = Math.toRadians(y);
		double c = Math.toRadians(z);
		return new Point(a, b, c);
	}

	public Point sub(Point relative) {
		double a = this.getX();
		double b = this.getY();
		double c = this.getZ();

		a -= relative.getX();
		b -= relative.getY();
		c -= relative.getZ();

		return new Point(a, b, c);
	}

	public double getPitch() {
		return pitch;
	}

	public void setPitch(double pitch) {
		this.pitch = pitch;
	}

}
