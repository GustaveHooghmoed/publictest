package me.rides.objects.Locations;

import org.bukkit.Location;

public class TrackLocation {

	private Location loc;
	private double roll;
	private double pitch;
	private double speed;
	private int index;

	public TrackLocation(int index, Point loc, double roll, double d) {
		this.setLoc(loc.loc());
		this.setRoll(roll);
		this.index = index;
		this.setPitch(0.0);
	}

	public TrackLocation(int index, Location add, double rolla) {
		this.loc = add;
		this.index = index;
		this.setRoll(rolla);
		this.setPitch(0.0);
	}

	public Location getLoc() {
		return loc;
	}

	public void setLoc(Location loc) {
		this.loc = loc;
	}

	public double getRoll() {
		return roll;
	}

	public void setRoll(double roll) {
		this.roll = roll;
	}

	public Location loc() {
		return this.loc;
	}

	public double getPitch() {
		return pitch;
	}

	public void setPitch(double pitch) {
		this.pitch = pitch;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public Point toPoint() {
		return new Point(loc.getX(),loc.getY(),loc.getZ());
	}

	public int getid() {
		return this.index;
	}

	public void setIndex(int ind) {
		this.index = ind;
	}
}