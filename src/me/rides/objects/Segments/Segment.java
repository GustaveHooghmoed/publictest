package me.rides.objects.Segments;

public class Segment {

	private int id;
	private String segment_name;
	private boolean bezet;
	private int pos;
	private int end;
	private SegmentType type;
	private double yaw;
	private long delay;
	private boolean draai;

	public Segment(int id, String name,int pos, int end, SegmentType type,
				   double i, boolean bezet) {
		this.id = id;
		this.segment_name = name;
		this.pos = pos;
		this.end = end;
		this.type = type;
		this.bezet = bezet;
		this.setYaw(i);
		this.setDelay(0);
		this.setDraai(false);
	}
	
	public Segment(int id, String name,int pos, int end, SegmentType type,
			   double i, long delay, boolean bezet) {
	this.id = id;
	this.segment_name = name;
	this.pos = pos;
	this.end = end;
	this.type = type;
	this.bezet = bezet;
	this.setYaw(i);
	this.setDelay(delay);
	this.setDraai(false);
}
	
	public Segment(int id, String name,int pos, int end, SegmentType type, boolean bezet) {
	this.id = id;
	this.segment_name = name;
	this.pos = pos;
	this.end = end;
	this.type = type;
	this.bezet = bezet;
	this.setYaw(0.0d);
	this.setDelay(0l);
	this.setDraai(false);
}

	public Segment(int id, String name,int pos, int end, SegmentType type,
			   double i, long delay,boolean draai, boolean bezet) {
		this.id = id;
		this.segment_name = name;
		this.pos = pos;
		this.end = end;
		this.type = type;
		this.bezet = bezet;
		this.setYaw(i);
		this.setDelay(delay);
		this.setDraai(draai);
	}

	public String getSegment_name() {
		return segment_name;
	}

	public void setSegment_name(String segment_name) {
		this.segment_name = segment_name;
	}

	public boolean isBezet() {
		return bezet;
	}

	public void setBezet(boolean bezet) {
		this.bezet = bezet;
	}



	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEndPos() {
		return this.end;
	}

	public SegmentType getType() {
		return this.type;
	}

	public double getYaw() {
		return yaw;
	}

	public void setYaw(double yaw) {
		this.yaw = yaw;
	}

	public long getDelay() {
		return this.delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public boolean isDraai() {
		return draai;
	}

	public void setDraai(boolean draai) {
		this.draai = draai;
	}

}
