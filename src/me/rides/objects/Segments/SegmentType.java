package me.rides.objects.Segments;

public enum SegmentType {

	STATION, LIFT, TRACK, BRAKES, PRELIFTHILL, STOP_BEFORE_DROP, YAW, YAW_STOP

}
