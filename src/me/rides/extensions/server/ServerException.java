package me.rides.extensions.server;

public class ServerException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ServerException() {
		
	}
	
	public ServerException(String exc) {
		super(exc);
	}

}
