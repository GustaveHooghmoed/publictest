package me.rides.interfaces;

import org.bukkit.entity.Player;

import me.rides.objects.Locations.TrackLocation;

public interface Ride {

    Train getTrain();

	TrackLocation getClosets(Player p);
}
