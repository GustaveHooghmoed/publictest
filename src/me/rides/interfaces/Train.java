package me.rides.interfaces;

import java.util.ArrayList;

import me.rides.Rides.fatamorgana.Ride.FataTrack;
import net.minecraft.server.v1_12_R1.EntityArmorStand;

public interface Train {

    void remove();

    String getName();

    float getYaw();

    int getPos();

    void move(int i, Track t);

    void moveWithYaw(int count, Track track, double yaw);

	ArrayList<EntityArmorStand> geEntityArmorStands();
}
