package me.rides.interfaces;

import java.util.ArrayList;

public interface Manager {

    void remove();

    void setup();

    ArrayList<Ride> getRides();

	void setSpeed(Integer valueOf);

	void setVrijgeven();
}
