package me.rides.interfaces;

import java.util.ArrayList;

import me.rides.objects.Locations.TrackLocation;

public interface Track {
	 void setup(double a,double b,double c);

	 ArrayList<TrackLocation> getTracklocations();
}

